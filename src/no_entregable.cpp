#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <string>

using namespace std;

ofstream output_file("salida.txt");

void print_solution(vector<int> v){
  if(!v.empty()){
    output_file << v[0];
    for(int i = 1; i < v.size(); i++)
      output_file << ", " << v[i];
    output_file << '\n';
  }
}

void find_solutions(list<int> numbers, int target, int curr_sum, vector<int> curr_sol){
  if(curr_sum == target){
    print_solution(curr_sol);
  }
  else if(curr_sum < target){
    if(!numbers.empty()){
      int curr_value = numbers.front();
      numbers.pop_front();
      find_solutions(numbers, target, curr_sum, curr_sol);//Not using curr_value
      curr_sol.push_back(curr_value);
      find_solutions(numbers, target, curr_sum + curr_value, curr_sol);//Using curr_value
    }
  }
}

int main(){

  list<int> numbers;
  vector<int> empty_vector;
  int input;

  while(cin >> input){
    numbers.push_back(input);
  }
  int target = numbers.back();
  numbers.pop_back();

  find_solutions(numbers, target, 0, empty_vector);

  return 0;
}
